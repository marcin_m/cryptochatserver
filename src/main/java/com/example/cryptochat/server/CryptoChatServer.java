package com.example.cryptochat.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class CryptoChatServer {

    public static void main(String[] args) {
        SpringApplication.run(CryptoChatServer.class, args);
    }
}
