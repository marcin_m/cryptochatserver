package com.example.cryptochat.server.chat.message;

import com.example.cryptochat.server.chat.Client;
import com.example.cryptochat.server.chat.CryptoChatException;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Map;

public interface CommonMessage {

    String getType();

    default void process(WebSocketSession session, Map<String, Client> clientStore) throws CryptoChatException, IOException {
    }
}
