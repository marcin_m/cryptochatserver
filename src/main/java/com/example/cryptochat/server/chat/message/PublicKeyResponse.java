package com.example.cryptochat.server.chat.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class PublicKeyResponse implements CommonMessage {

    private final String type = "PUBLIC_KEY_RESPONSE";

    private String user;

    private String messageId;

    private String publicKey;

    @Override
    public String getType() {
        return type;
    }
}
