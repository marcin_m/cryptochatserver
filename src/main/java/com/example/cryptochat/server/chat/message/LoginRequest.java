package com.example.cryptochat.server.chat.message;

import com.example.cryptochat.server.chat.Client;
import com.example.cryptochat.server.chat.CryptoChatException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class LoginRequest implements CommonMessage {

    private final String type = "LOGIN";

    private String user;

    private String publicKey;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void process(WebSocketSession session, Map<String, Client> clientStore) throws CryptoChatException {
        log.info("Trying to login user = {}", user);
        if (userExists(clientStore)) {
            throw new CryptoChatException("User already exists");
        } else {
            Client client = new Client(user, publicKey, session);
            clientStore.put(session.getId(), client);
            log.info("User {} created successfully", client.getUser());
        }
    }

    private boolean userExists(Map<String, Client> store) {
        return store.values().stream().anyMatch(client -> client.getUser().equals(user));
    }
}
