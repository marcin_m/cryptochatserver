package com.example.cryptochat.server.chat.message;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorMessage implements CommonMessage {

    private final String type = "ERROR";

    private String message;

    @Override
    public String getType() {
        return type;
    }
}
