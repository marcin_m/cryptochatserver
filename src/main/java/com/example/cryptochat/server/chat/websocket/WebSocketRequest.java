package com.example.cryptochat.server.chat.websocket;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WebSocketRequest {

    private String type;
}
