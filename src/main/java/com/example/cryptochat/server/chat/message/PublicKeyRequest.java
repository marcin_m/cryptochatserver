package com.example.cryptochat.server.chat.message;

import com.example.cryptochat.server.chat.Client;
import com.example.cryptochat.server.chat.CryptoChatException;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class PublicKeyRequest implements CommonMessage {

    private final String type = "PUBLIC_KEY_REQUEST";

    private String messageId;

    private String user;

    private Gson gson = new Gson();

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void process(WebSocketSession session, Map<String, Client> clientStore) throws CryptoChatException, IOException {
        String publicKey = getUserPublicKey(clientStore);
        PublicKeyResponse publicKeyResponse = new PublicKeyResponse(user, messageId, publicKey);
        clientStore.get(session.getId()).sendMessage(publicKeyResponse);
    }

    private String getUserPublicKey(Map<String, Client> clientStore) throws CryptoChatException {
        Client client = clientStore
                .values()
                .stream()
                .filter(c -> c.getUser().equals(user))
                .findFirst()
                .orElseThrow(() -> new CryptoChatException("Requested client does not exist"));
        return client.getPublicKey();
    }
}
