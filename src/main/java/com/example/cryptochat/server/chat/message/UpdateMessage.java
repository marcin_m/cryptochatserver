package com.example.cryptochat.server.chat.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class UpdateMessage implements CommonMessage {

    private final String type = "UPDATE";

    private List<String> users;

    @Override
    public String getType() {
        return type;
    }
}
