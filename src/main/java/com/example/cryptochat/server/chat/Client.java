package com.example.cryptochat.server.chat;

import com.example.cryptochat.server.chat.message.CommonMessage;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

@Slf4j
@Data
@AllArgsConstructor
public class Client {

    private String user;

    private String publicKey;

    private WebSocketSession session;

    private final Gson gson = new Gson();

    public void sendMessage(CommonMessage commonMessage) throws IOException {
        log.info("Sending message, type = {}, sessionId = {}", commonMessage.getType(), session.getId());
        String updateMessageJson = gson.toJson(commonMessage);
        TextMessage message = new TextMessage(updateMessageJson);
        session.sendMessage(message);
    }
}
