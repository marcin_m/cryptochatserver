package com.example.cryptochat.server.chat;

import com.example.cryptochat.server.chat.message.CommonMessage;
import com.example.cryptochat.server.chat.message.ErrorMessage;
import com.example.cryptochat.server.chat.websocket.WebSocketRequest;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class ClientHandler extends TextWebSocketHandler implements ApplicationContextAware {

    private Gson gson = new Gson();

    private Map<String, CommonMessage> messageTypes = new HashMap<>();

    private ApplicationContext applicationContext;

    private HashMap<String, Client> clientStore = new HashMap<>();

    @PostConstruct
    public void init() {
        Map<String, CommonMessage> beansOfType = applicationContext.getBeansOfType(CommonMessage.class);
        beansOfType.forEach((k, v) -> messageTypes.put(v.getType(), v));
        log.info("Init message types {}", messageTypes);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("New session, id = {}", session.getId());
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        try {
            handle(session, message);
        } catch (CryptoChatException cryptoChatException) {
            log.error("Error while processing message = {}", cryptoChatException.getMessage());
            ErrorMessage errorMessage = new ErrorMessage(cryptoChatException.getMessage());
            clientStore.get(session.getId()).sendMessage(errorMessage);
            session.close();
        } catch (Exception e) {
            log.error("Error while handling message, for client = {}", session.getId(), e);
            throw e;
        }
    }

    private void handle(WebSocketSession session, TextMessage message) throws IOException, CryptoChatException {
        log.trace("New message = {}", message.getPayload());
        WebSocketRequest webSocketRequest = gson.fromJson(message.getPayload(), WebSocketRequest.class);
        String messageType = webSocketRequest.getType();
        log.info("Message type = {}, sessionId = {}", messageType, session.getId());

        Class<? extends CommonMessage> messageHandlerClass = messageTypes.get(messageType).getClass();
        CommonMessage commonMessage = gson.fromJson(message.getPayload(), messageHandlerClass);

        commonMessage.process(session, clientStore);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        clientStore.remove(session.getId());
        log.info("Session ended, id = {}", session.getId());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public Map<String, Client> getClientStore() {
        return clientStore;
    }
}
