package com.example.cryptochat.server.chat.message;

import com.example.cryptochat.server.chat.Client;
import com.example.cryptochat.server.chat.CryptoChatException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class MessageOut implements CommonMessage {

    private final String type = "MESSAGE_OUT";

    private String to;

    private String messageId;

    private String messageContent;

    private Date date;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void process(WebSocketSession session, Map<String, Client> clientStore) throws CryptoChatException, IOException {
        String from = clientStore.get(session.getId()).getUser();
        Client targetClient = clientStore
                .values()
                .stream()
                .filter(c -> c.getUser().equals(to))
                .findFirst()
                .orElseThrow(() -> new CryptoChatException("Target client does not exist"));
        targetClient.sendMessage(new MessageIn(from, messageId, messageContent, date));
    }
}