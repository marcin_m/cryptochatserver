package com.example.cryptochat.server.chat.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class MessageIn implements CommonMessage {

    private final String type = "MESSAGE_IN";

    private String from;

    private String messageId;

    private String messageContent;

    private Date date;

    public String getType() {
        return type;
    }
}
