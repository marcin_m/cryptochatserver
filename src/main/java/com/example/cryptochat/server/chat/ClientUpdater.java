package com.example.cryptochat.server.chat;

import com.example.cryptochat.server.chat.message.UpdateMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ClientUpdater {

    @Autowired
    private ClientHandler clientHandler;

    @Scheduled(fixedRate = 1000)
    private void sendUpdate() throws IOException {
        Map<String, Client> clientStore = clientHandler.getClientStore();
        log.debug("Sending update for {} users", clientStore.size());
        List<String> users = clientStore.values().stream().map(Client::getUser).collect(Collectors.toList());
        UpdateMessage updateMessage = new UpdateMessage(users);
        for (Client client : clientStore.values()) {
            client.sendMessage(updateMessage);
        }
    }
}
