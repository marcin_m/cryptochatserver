package com.example.cryptochat.server.chat;

public class CryptoChatException extends Exception {

    public CryptoChatException(String message) {
        super(message);
    }
}
