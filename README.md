# CryptoChat Server

This is a server application for CryptoChat - an instant messaging app, created as an example of end-to-end encryption communication. 

**Under any circumstance, do not use it as a full-featured secure communicator!** It is merely an educational app. 

In order to connect to server, use [client application](https://bitbucket.org/mj_kowalski/cryptochatclient) 

## Running server directly from maven

```
mvn spring-boot:run

```

## Building docker image

```
mvn install docker:build

```

## Running server from docker

```
docker run -p 8080:8080 -t cryptochat.gq/cryptochat-server
```

### Requirements
* Java8 SDK
* Maven
* Optional: [Lombok plugin](https://projectlombok.org/), if you wish to use IDE such as IntelliJ IDEA, or Eclipse 